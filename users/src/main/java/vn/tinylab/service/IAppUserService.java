package vn.tinylab.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.tinylab.model.entity.AppUser;

public interface IAppUserService {
	AppUser createNewUser(AppUser usr);
	List<AppUser> loadUserByUsername(String Username);
	Page<AppUser> findAll(Pageable pageable);
	Page<AppUser> findByEmailContaining(String email, Pageable pageable);
	AppUser findById(Long id);
	AppUser updateUser(Long id, AppUser newAppUser);
	void deleteAppUser(Long id);
}
