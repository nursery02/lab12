package vn.tinylab.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.tinylab.model.entity.BOL;


public interface IBOLService {
	BOL createNew(BOL usr);
	BOL findById(Long id);
}
