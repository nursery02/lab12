package vn.tinylab.model.response;

import java.io.Serializable;

public class ResponseModel<T> implements Serializable{

	private static final long serialVersionUID = -8091879091924046844L;
	private T data;
	
	private Boolean success = false;
	private String errorMessage = "";

	public Boolean getSuccess() {
		return success;
	}
	
	public ResponseModel(Boolean success, T oData) {
		this.data = oData;
		this.success = success;
		this.errorMessage = "";
	}

	public ResponseModel(Boolean success, T oData, String errorMessage) {
		this.data = oData;
		this.success = success;
		this.errorMessage = errorMessage;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public ResponseModel() {
		this.data = null;
		// TODO Auto-generated constructor stub
	}

	public T getData() {
		return this.data;
	}
	
	public void setData(T data1) {
		this.data = data1;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}
