package vn.tinylab.model.dto;

import java.io.Serializable;

import vn.tinylab.model.entity.BOL;

public class BOLDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private BOL bol;
	private String note;
	
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public BOL getBol() {
		return bol;
	}

	public void setBol(BOL bol) {
		this.bol = bol;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	private UserDTO user;
}
