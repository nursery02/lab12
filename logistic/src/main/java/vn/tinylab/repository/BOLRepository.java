package vn.tinylab.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.tinylab.model.entity.BOL;


public interface BOLRepository extends JpaRepository<BOL, Long>  {
}
