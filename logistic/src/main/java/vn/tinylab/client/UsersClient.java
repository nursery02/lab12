package vn.tinylab.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import vn.tinylab.model.dto.UserDTO;
import vn.tinylab.model.response.ResponseModel;

@Service
public class UsersClient {
    public static String serviceUrl = "http://user-service";

    @Autowired
    private RestTemplate restTemplate;

    public UserDTO getUserById(long userId) {
        String serviceUrl = "http://user-service";
        ResponseEntity<?> res = restTemplate.getForEntity(serviceUrl + "/users/" + userId, ResponseModel.class);
        ObjectMapper mapper = new ObjectMapper();
        ResponseModel<UserDTO> responseModel = mapper.convertValue(res.getBody(), new TypeReference<>() {});
        return responseModel.getData();
    }

    public String getInstanceName() {
        String serviceUrl = "http://user-service";
        ResponseEntity<?> res = restTemplate.getForEntity(serviceUrl + "/users/instance-name", ResponseModel.class);
        ObjectMapper mapper = new ObjectMapper();
        ResponseModel<String> responseModel = mapper.convertValue(res.getBody(), new TypeReference<>() {});
        return responseModel.getData();
    }
}
